## Edukamu ohjelmointitehtävä 04/2022

### Vaaditut ohjelmistot

- Node.js
- Tekstieditori, esim. VSCode

### Testiympäristön käyttöönotto

Suorita `npm install`, jonka jälkeen React sovelluksen saat käyttöön suorittamalla `npm start` ja Express backendin kommennolla `npm run start:backend`.

### Tehtävät

Voit suorittaa halutessasi kaikki tehtävät, mutta vain ensimmäinen vaaditaan. Mitä useamman tehtävän suoritat, sen parempi.

1. Luo Reactiin komponentti, joka lukee tiedot `src/data/users.json` tiedostosta ja renderöi näistä listan, jossa näytetään kaikki käyttäjät ja heidän tietonsa.
   - Muokkaa listan ulkoasu mieleiseksesi CSS:n avulla
   - Tiedot voidaan hakea myös ulkoisesta päätepisteestä, joka löytyy `endpoint` muuttujasta `App.js` tiedostosta.
2. Luo itsellesi päätepiste `backend/index.js` tiedostoon, joka hakee käyttäjätiedot ulkoisesta päätepisteestä. Luomasi päätepiste palauttaa saadun tiedon.
3. Käytä luomaasi Express päätepistettä tiedonhakuun React:ssa, josta lista renderöidään.

### Tehtävän palautus

Puske omat muutokset forkaamaasi repositoryyn ja lähetä repositoryn linkki osoitteeseen `jonna.kalermo-poranen@kamk.fi`.

### Dokumentaatiot

Apua saat tarvittaessa seuraavista lähteistä:

- https://reactjs.org/docs/getting-started.html
- https://expressjs.com/en/4x/api.html