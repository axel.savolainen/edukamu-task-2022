import logo from "./logo.svg";
import "./App.css";
import json from "./data/users.json";
import JsonDataDisplay from "./DisplayData";

// TODO: tarvittavat React-komponentit


function App() {
  const endpoint = "https://europe-west1-dev-edukamu.cloudfunctions.net/users";

  console.log(json.data);

  return (
    <div className="App">
      <h1>Käyttäjätiedot</h1>
      <JsonDataDisplay/>


    </div>
  );
}

export default App;
