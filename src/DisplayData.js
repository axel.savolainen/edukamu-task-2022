import JsonData from "./data/users.json";
import React from 'react';
// TODO: tarvittavat React-komponentit

function JsonDataDisplay(){
  const DisplayData=JsonData.data.map(
    (info)=>{
      return(
        <tr>
          <td>{info.name}</td>
          <td>{info.email}</td>
          <td>{info.address}</td>
        </tr>
      )
    }
  )
  
  return(
    <div>
      <table class="table table.striped table-dark">
        <thead>
          <tr>
            <th scope="col">Name</th>
            <th scope="col">Email</th>
            <th scope="col">Address</th>
          </tr>
        </thead>
        <tbody>


          {DisplayData}

        </tbody>
      </table>
    </div>
  )
}

export default JsonDataDisplay;