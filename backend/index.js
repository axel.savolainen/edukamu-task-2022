const express = require("express");
const app = express();
const port = 5000;
const endpoint = "https://europe-west1-dev-edukamu.cloudfunctions.net/users";

/**
 * TODO: Endpoints
 */

/**
 * Setup
 */
app.listen(port, () => {
  console.log(`Listening on port ${port}`);
});
